// Footer.js
import React from 'react';

const Footer = () => {
  return (
    <footer className="footer">
      <p>&copy; {new Date().getFullYear()} My Stylish Home Page. All rights reserved.</p>
    </footer>
  );
};

export default Footer;
