// HomePage.js
import React from 'react';
import './Home.css';
import Footer from './Footer';

const Home = () => {
  return (
    <div className="home-page">
      <header className="header">
        <h1>InternNexus</h1>
        <p>Lets, Begin Your Carrier With Us</p>
      </header>
      <main className="main">
        <section className="section-one">
          <h2 align="center">Learn The Trending Technology</h2>
          <br />
          <br />

          <div className="box-container">
            <div className="box">
              <div className="box-img">
                <img src="https://beta-test.internnexus.com/assets/angularsvg.svg" alt="img" />
              </div>
            </div>
            <div className="box">
              <div className="box-img">
                <img src="https://beta-test.internnexus.com/assets/node-js.svg" alt="img" />
              </div>
            </div>
            <div className="box"><div className="box-img">
              <img src="https://beta-test.internnexus.com/assets/python.svg" alt="img" />
            </div></div>
            <div className="box">
              <div className="box-img">
                <img src="https://beta-test.internnexus.com/assets/angularsvg.svg" alt="img" />
              </div>
            </div>
            <div className="box">
              <div className="box-img">
                <img src="https://beta-test.internnexus.com/assets/node-js.svg" alt="img" />
              </div>
            </div>
            <div className="box"><div className="box-img">
              <img src="https://beta-test.internnexus.com/assets/python.svg" alt="img" />
            </div></div>
          </div>

        </section>
        <section className="section-two">
          <h2 align="center">Rewards & Recognition</h2>
          <br /><br />
          <p>
          Great news! Internnexs has been selected as one of the 25 start-ups to be shortlisted for rent-free office space in Bihar's Start-Up Hub. This is a fantastic opportunity for the company to grow and expand its operations in a supportive and nurturing environment
            The CEO of InternNexus has been invited to share their expertise on "Technology-based Entrepreneurship" as a guest speaker at Dibrugarh University. This is an exciting opportunity to learn from a successful entrepreneur who has used technology to build a thriving business.
          </p>
        </section>
      </main>
      <footer className="footer">
        <p>&copy; {new Date().getFullYear()} My Stylish Home Page. All rights reserved.</p>
      </footer>

    </div>

  );
};

export default Home;
